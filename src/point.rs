extern crate rand;

use std::fmt;

#[derive(Copy)]
pub struct Point {
    pub x: isize,
    pub y: isize,
}


impl Point {
    pub fn random(min: Point, max: Point) -> Point {
        let mut rng = rand::thread_rng();
        Point{x: ((rng.gen::<isize>()).abs() % (max.x - min.x)) + min.x,
              y: ((rng.gen::<isize>()).abs() % (max.y - min.y)) + min.y}
    }

    pub fn add(&self, origin: Point) -> Point {
        Point {
            x: self.x + origin.x,
            y: self.y + origin.y,
        }
    }

    pub fn sub(&self, p: Point) -> Point {
        Point {
            x: self.x - p.x,
            y: self.y - p.y,
        }
    }

    pub fn mv(&mut self, origin: Point) {
        self.x += origin.x;
        self.y += origin.y;
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}x{}", self.x, self.y)
    }
}
