extern crate rustbox;
use rustbox::{Color, RustBox};

mod point;
use point::Point;

mod direction;
use direction::Direction;
use direction::Direction::{Up, Down, Left, Right};

mod rbgui;

/*mod simple_random;
use simple_random::SimpleRandom;
*/

use std::char;
use std::error::Error;
use std::io;
use std::thread;
use std::time::Duration;


struct Snake {
    head:          Point,
    body:          Vec<Point>,
    previous_tail: Point,
    ch:            char,
    direction:     Direction,
}

impl Snake {
    fn init(head: Point, d: Direction) -> Snake {
        Snake {
            head:          head,
            body:          vec![],
            previous_tail: head.add(d.opposite().as_point()),
            ch:            'x',
            direction:     d
        }
    }

    fn draw(&self, rb: &RustBox, topright: Point) {
        for p in self.body.iter().rev() {
            let pt = p.add(topright);
            rb.print_char(pt.x as usize, pt.y as usize, rustbox::RB_NORMAL, Color::Blue, Color::Black, self.ch);
        }
        let previous_tail = self.previous_tail.add(topright);
        rb.print_char(previous_tail.x as usize, previous_tail.y as usize, rustbox::RB_NORMAL, Color::Black, Color::Black, ' ');
        let head = self.head.add(topright);
        rb.print_char(head.x as usize, head.y as usize, rustbox::RB_BOLD, Color::Blue, Color::Black, self.ch);
    }

    fn explode(&self, rb: &RustBox, topright: Point) {
        for r in 0..10 {
            let Point{x, y} = self.head.add(topright);
            rbgui::print_circle(rb, x as usize, y as usize, r, rustbox::RB_BOLD, Color::Blue, Color::Black, self.ch);
            rb.present();
            thread::sleep(Duration::milliseconds(100));
            rbgui::print_circle(rb, x as usize, y as usize, r, rustbox::RB_NORMAL, Color::Black, Color::Black, ' ');
        }
    }

    fn forward(&mut self) {
        if self.body.len() > 0 {
            self.body.insert(0, self.head);
        }
        self.head.mv(self.direction.as_point());
        self.previous_tail = match self.body.pop() {
            Some(p) => p,
            None => self.head.add(self.direction.opposite().as_point()),
        }
    }

    fn grow(&mut self) {
        self.body.push(self.previous_tail);
        let len = self.body.len();
        // len > 0 because we pushed something in it
        let tail = self.body[len - 1];
        // we get the direction of the new tail
        let diff = if len > 1 {
            tail.sub(self.body[len - 2])
        } else {
            tail.sub(self.head)
        };
        self.previous_tail = tail.add(diff);
    }

    fn eats_itself(&self) -> bool {
        for p in self.body.iter() {
            if *p == self.head {
                return true;
            }
        }
        false
    }

    fn change_direction(&mut self, new_direction: Direction) {
        if self.body.is_empty() || new_direction != self.direction.opposite() {
            self.direction = new_direction;
        }
    }

    fn on_point(&self, p: Point) -> bool {
        for pt in self.body.iter() {
            if *pt == p { return true; }
        }
        p == self.head
    }
}


struct Bonus {
    pos: Point,
    ch: char,
    value: u8,
}

impl Bonus {
    fn init(score: u8) -> Bonus {
        Bonus {
            pos: Point{x: -1, y: -1},
            ch: 'o',
            value: score,
        }
    }

    fn change_position(&mut self, p: Point) {
        self.pos = p;
    }

    fn draw(&self, rb: &RustBox, topright: Point) {
        let p = self.pos.add(topright);
        rb.print_char(p.x as usize, p.y as usize, rustbox::RB_NORMAL, Color::Green, Color::Black, self.ch);
    }
}


struct GameBoard {
    height:  usize,
    width:   usize,
    topleft: Point,
    snake:   Snake,
    bonus:   Bonus,
    paused:  bool,
    score:   u8,
    speed:   Duration,
}

impl GameBoard {
    fn init(x: isize, y: isize, width: usize, height: usize) -> GameBoard {
        let snake_pos = Point{x: (width / 2) as isize, y: (height / 2) as isize};
        let snake_direction = Up;
        let mut res = GameBoard {
            height: height,
            width: width,
            topleft: Point{x: x, y: y},
            snake: Snake::init(snake_pos, snake_direction),
            bonus: Bonus::init(42),
            paused: false,
            score: 0,
            speed: Duration::seconds(1),
        };
        res.relocate_bonus();
        res
    }

    fn key_pressed(&mut self, key: u16, ch_u: char) {
        if ch_u == 'p' {
            self.paused = !self.paused;
        }
        if !self.paused {
            match (key, ch_u) {
                (65514, _) => self.snake.change_direction(Right),
                (65515, _) => self.snake.change_direction(Left),
                (65516, _) => self.snake.change_direction(Down),
                (65517, _) => self.snake.change_direction(Up),
                (_, _) => {}
            }
        }
    }

    fn compute(&mut self) {
        if !self.paused {
            self.snake.forward();
            if self.snake.head == self.bonus.pos {
                self.score += self.bonus.value;
                self.snake.grow();
                self.bonus = Bonus::init(42);
                self.relocate_bonus();
                self.speed = self.speed - self.speed / 10;
            }
        }
    }

    fn relocate_bonus(&mut self) {
        let mut p = Point::random(self.topleft, Point{x: self.width as isize, y: self.height as isize});
        while self.snake.on_point(p) {
            p = Point::random(self.topleft, Point{x: self.width as isize, y: self.height as isize});
        }
        self.bonus.change_position(p);
    }

    fn is_over(&self) -> bool {
        (self.snake.eats_itself() ||
         self.snake.head.x < 0 || self.snake.head.x as usize >= self.width ||
         self.snake.head.y < 0 || self.snake.head.y as usize >= self.height)
    }

    fn draw(&self, rb: &RustBox) {
        let pause_text = if self.paused {
            ["*********",
             "* PAUSE *",
             "*********"]
        } else {
            ["         ",
             "         ",
             "         "]
        };
        let pause_height: usize = pause_text.len();
        let mut center = Point{x: (self.width / 2) as isize, y: (self.height / 2) as isize};
        let pause_recenter = Point{x: - ((pause_text[0].len() / 2) as isize),
                                   y: - ((pause_height / 2) as isize)};
        center.add(self.topleft);
        center.mv(pause_recenter);
        for i in 0..pause_height {
            rb.print((center.x - 4) as usize, (center.y as usize) + i, rustbox::RB_BOLD, Color::Green, Color::Black, pause_text[i]);
        }

        if !self.paused {
            self.snake.draw(&rb, self.topleft);
            self.bonus.draw(&rb, self.topleft);
        }
    }

    fn end_animation(&self, rb: &RustBox) {
        self.snake.explode(&rb, self.topleft);
    }

}


struct DebugLine {
    x:     usize,
}

impl DebugLine {
    fn init() -> DebugLine {
        DebugLine {
            x:     0,
        }
    }

    fn draw(&mut self, rb: &RustBox, message: String) {
        let y = rb.height() - 1;
        let debug_info = message; //format!("[{}]", /*"[key: KEY_PRESSED | snake: SNAKE_STRUCT_REPR]"*/rb.width(), if self.x < rb.width() { self.x } else { 0 }, self.right);
        rb.print(self.x as usize, y as usize, rustbox::RB_BOLD, Color::White, Color::Black,
                 format!(" {}            ", debug_info).as_slice());
    }
}

struct App {
    game: GameBoard,
    rbx: RustBox,
//    dbgl: DebugLine,
    last_event: rustbox::Event,
}

impl App {
    fn init() -> App {
        let rbx = match RustBox::init(Default::defailt()) {
            Ok(v) => v,
            Err(e) => panic!("{}", e),
        };

        App {
            game: GameBoard::init(0, 0, rbx.width(), rbx.height()),
            rbx: rbx,
            //            dbgl: DebugLine::init(),
            last_event: rustbox::Event::NoEvent,
        }


    }

    fn get_last_event(&mut self) {
        self.last_event = match self.rbx.peek_event(Duration::milliseconds(0)) {
            Ok(e) => e,
            Err(e) => panic!("{}", e.description()),
        };
    }

    fn run(&mut self) {
        loop {
            /********** Get inputs **********/

            self.last_event = rustbox::Event::NoEvent;
            self.get_last_event();

            // Note for future management with threads:
            // try to receive the event from the channel
            // if nothing, set last_rustbox_event to Rustbox::Event::NoEvent;

            /********** manage inputs ***********/

//            let mut last_pressed_key = String::new();
            match self.last_event {
                rustbox::Event::KeyEvent(_, key, ch) => {
                    let ch_u = match char::from_u32(ch) {
                        Some(c) => c,
                        _       => ' ',
                    };

//                    last_pressed_key = format!("{} {}({})", key, ch, ch_u);

                    match (key, ch_u) {
                        (_, 'q') |
                        (27, _)    => { break; },
//                        (_, 'l')   => { self.game.speed = self.game.speed * 2;},
//                        (_, 'm')   => { self.game.speed = self.game.speed / 2},
                        (_, _)     => self.game.key_pressed(key, ch_u),
                    }
                },
                rustbox::Event::ResizeEvent(w, h) => {
                    // game.resize(w, h);
                    println!("window size changed {} {}", w, h);
                },
                _ => { }
            }

            /********** compute game **********/

            self.game.compute();

            if self.game.is_over() {
                break;
            }


            /********** Draw **********/
            self.game.draw(&self.rbx);
//            self.dbgl.draw(&self.rbx, format!("w,h={},{} | t={} | k={} | s={}_{} | b={}", self.rbx.width(), self.rbx.height(), self.game.speed, last_pressed_key, self.game.snake.body.len() + 1, self.game.snake.head, self.game.bonus.pos));
            self.rbx.present();
            thread::sleep(self.game.speed);
        }
        self.game.end_animation(&self.rbx);

    }
}


fn main() {

    let mut app = App::init();

/*    let mut dbgl = DebugLine::init();
    app.register_renterable(dbgl);
*/
    app.run();

    let score = app.game.score;
    drop(app);

    println!("Game over !");
    println!("Final score: {}", score);

}
