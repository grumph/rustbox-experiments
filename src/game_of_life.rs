mod game_of_life_data;
use game_of_life_data as Data;

mod simple_random;
use simple_random::SimpleRandom;

extern crate rustbox;
extern crate rand;

use rustbox::{Color, RustBox, InitOptions};

use std::char;
use std::error::Error;
use std::io;
use std::time::Duration;



struct DebugLine {
    x:     usize,
    right: bool,
}

impl DebugLine {
    fn init() -> DebugLine {
        DebugLine {
            x:     0,
            right: true,
        }
    }

    fn draw(&mut self, rb: &RustBox, message: String) {
        let y = rb.height() - 1;
        let debug_info = message;
        rb.print(self.x, y, rustbox::RB_BOLD, Color::White, Color::Black,
                 format!(" {}            ", debug_info).as_slice());


        if self.right && self.x + debug_info.len() >= rb.width() - 1 {
            self.right = false;
        } else if !self.right && (self.x > rb.width() /* || self.x == 0 */) {
            self.right = true;
        }
        self.x = if self.right {
            self.x + 1
        } else {
            self.x - 1
        }
    }
}

struct Board {
    width:  usize,
    height: usize,
    tab:    Vec<bool>,
}


impl Board {
    fn init(width: usize, height: usize) -> Board {
        let mut res = Board {
            width:  0,
            height: 0,
            tab:    Vec::new(),
        };
        res.resize(width, height);
        res
    }


    fn populate(&mut self) {
        let mut rng = rand::thread_rng();
        for t in self.tab.iter_mut() {
            *t = rng.gen();
        }
    }

    fn populate_from_seed(&mut self, seed: u32) {
        let mut sr = SimpleRandom::new(seed);
        for t in self.tab.iter_mut() {
            *t = sr.gen() % 2 == 1;
        }
    }

    fn populate_from_vec(&mut self, width: usize, v: Vec<bool>) {
        let height = v.len() / width;
        let top = self.height / 2 - height / 2;
        let left = self.width / 2 - width / 2;
        for y in 0..height {
            for x in 0..width {
                self.tab[x + left + (y + top) * self.width] = v[x + y * width];
            }
        }
    }

    fn resize(&mut self, width: usize, height: usize) {
        // first reduce height if needed
        if height < self.height {
            self.tab.resize(self.width * height, false);
            self.height = height;
        }

        // then play with width and insert
        for y in (0..self.height).rev() {
            if width < self.width {
                for x in (width..self.width).rev() {
                    self.tab.remove(x + y * self.width);
                }
            } else if width > self.width {
                for _ in self.width..width {
                    self.tab.insert(self.width - 1 + (y) * self.width, false);
                }
            }
        }
        self.width = width;

        // now grow height if needed
        if height > self.height {
            self.tab.resize(self.width * height, false);
            self.height = height;
        }
    }

    fn draw(&self, rb: &RustBox) {
        for i in 0..self.tab.len() {
            rb.print_char(i % self.width,
                          (i - i % self.width) / self.width,
                          rustbox::RB_BOLD,
                          Color::Blue,
                          Color::Black,
                          if self.tab[i] {'0'} else {' '});
        }
    }

    fn count_neighbours(&self, x: usize, y: usize) -> u8 {
        let index = x + y * self.width;
        let mut res = 0;

        let top    = y > 0;
        let bottom = y < (self.height - 1);
        let left   = x > 0;
        let right  = x < (self.width - 1);
        if right && self.tab[index + 1] {
            res += 1;
        }
        if right && bottom && self.tab[index + 1 + self.width] {
            res += 1;
        }
        if right && top && self.tab[index + 1 - self.width] {
            res += 1;
        }
        if left && self.tab[index - 1] {
            res += 1;
        }
        if left && bottom && self.tab[index - 1 + self.width] {
            res += 1;
        }
        if left && top && self.tab[index - 1 - self.width] {
            res += 1;
        }
        if bottom && self.tab[index + self.width] {
            res += 1;
        }
        if top && self.tab[index - self.width] {
            res += 1
        }

        res
    }

    fn process(&mut self) {
        let mut next_tab = self.tab.clone();
        for i in 0..self.tab.len() {
            next_tab[i] = match (self.tab[i], self.count_neighbours(i % self.width, (i - i % self.width) / self.width)) {
                (true,  n) if n < 2 || n > 3 => false,
                (true,  n) if n > 1 && n < 4 => true,
                (false, 3)                   => true,
                _                            => false,
            };
        }
        for i in 0..self.tab.len() {
            self.tab[i] = next_tab[i];
        }
    }
}



fn main() {
    let options = [
        if io::stderr().isatty() { Some(InitOptions::BufferStderr) } else { None },
        ];

    let rbx = RustBox::init(&options).ok().unwrap();

    let mut board = Board::init(rbx.width(), rbx.height() - 1);
    // board.populate();
    board.populate_from_seed(42);
    // let pattern = Data::get_pattern(Data::Pattern::Pulsar);
    // board.populate_from_vec(pattern.0, pattern.1);

    let mut t = Duration::milliseconds(250);
    let mut dbgl = DebugLine::init();
    let mut last_pressed_key = String::new();

    loop {
        rbx.present();
        match rbx.peek_event(t) {
            Ok(rustbox::Event::KeyEvent(_, key, ch)) => {
                let ch_u = match char::from_u32(ch) {
                    Some(c) => c,
                    _       => ' ',
                };

                last_pressed_key = format!("{} {}({})", key, ch, ch_u);

                match (key, ch_u) {
                    (_, 'q') | (27, _) => { break; },
                    (_, 'l')   => { t = t * 2;},
                    (_, 'm')   => { t = t / 2},
                    (_, _)   => {}
                }
            },
            Ok(rustbox::Event::ResizeEvent(w, h)) => {
                board.resize(w as usize, (h - 1) as usize);
                dbgl.x = 0;
                /* FIXME : redefine the game board */
            },
            Err(e) => panic!("{}", e.description()),
            _ => { }
        }
        board.draw(&rbx);
        dbgl.draw(&rbx, format!("w={} | h={} | t={} | k={}", rbx.width(), rbx.height(), t, last_pressed_key));
        board.process();
    }
}
