
pub struct SimpleRandom {
    seed: u32,
}


impl SimpleRandom {
    pub fn random() -> u32 {
        SimpleRandom::new(42).gen()
    }

    pub fn new(seed: u32) -> SimpleRandom {
        SimpleRandom {
            seed: seed,
        }
    }

    pub fn gen(&mut self) -> u32 {
        self.seed = (214013 * self.seed + 2531011) & 0x7fffffff;
        self.seed >> 16
    }
}
