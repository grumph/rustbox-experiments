
use point::Point;

use direction::Direction::{Up, Down, Left, Right};


#[derive(Copy)]
pub enum Direction {
    Right,
    Left,
    Down,
    Up,
}

impl Direction {
    pub fn opposite(&self) -> Direction {
        match *self {
            Up => Down,
            Down => Up,
            Left => Right,
            Right => Left
        }
    }

    pub fn as_point(&self) -> Point {
        match *self {
            Up    => Point{x: 0, y:-1},
            Down  => Point{x: 0, y: 1},
            Left  => Point{x:-1, y: 0},
            Right => Point{x: 1, y: 0},
        }
    }

    pub fn from_point(p: Point) -> Result<Direction, &'static str> {
        match p {
            Point{x: 0, y: n} if n < 0 => Ok(Up),
            Point{x: 0, y: n} if n > 0 => Ok(Down),
            Point{x: n, y: 0} if n < 0 => Ok(Left),
            Point{x: n, y: 0} if n > 0 => Ok(Right),
            _ => Err("Point is not a valid direction")
        }
    }
}

impl PartialEq for Direction {
    fn eq(&self, other: &Direction) -> bool {
        match (self, other) {
            (&Right, &Right) |
            (&Left, &Left)   |
            (&Down, &Down)   |
            (&Up, &Up)       => true,
            _                => false,
        }
    }
}
