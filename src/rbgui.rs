extern crate rustbox;

use rustbox::{Color, RustBox};
use rustbox::Style;

pub fn print_circle(rb: &RustBox, x0: usize, y0: usize, r: usize, sty: Style, fg: Color, bg: Color, ch: char) {
    let mut f: i16= 1 - r as i16;
    let mut ddf_x: i16 = 1;
    let mut ddf_y: i16 = -2 * r as i16;
    let mut x = 0;
    let mut y = r;

    rb.print_char(x0 + r, y0    , sty, fg, bg, ch);
    rb.print_char(x0 - r, y0    , sty, fg, bg, ch);
    rb.print_char(x0    , y0 + r, sty, fg, bg, ch);
    rb.print_char(x0    , y0 - r, sty, fg, bg, ch);

    while x < y {
        if f >= 0 {
            y -= 1;
            ddf_y += 2;
            f += ddf_y;
        }
        x += 1;
        ddf_x += 2;
        f += ddf_x;

        rb.print_char(x0 + x, y0 + y, sty, fg, bg, ch);
        rb.print_char(x0 - x, y0 + y, sty, fg, bg, ch);
        rb.print_char(x0 + x, y0 - y, sty, fg, bg, ch);
        rb.print_char(x0 - x, y0 - y, sty, fg, bg, ch);

        rb.print_char(x0 + y, y0 + x, sty, fg, bg, ch);
        rb.print_char(x0 - y, y0 + x, sty, fg, bg, ch);
        rb.print_char(x0 + y, y0 - x, sty, fg, bg, ch);
        rb.print_char(x0 - y, y0 - x, sty, fg, bg, ch);
    }
}


