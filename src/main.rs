extern crate rustbox;

use std::char;
use std::old_io::stdio;
use std::error::Error;

use rustbox::{Color, RustBox, InitOption};

use std::rand::random;
use std::time::duration::Duration;

fn draw_quit_text(rb: &RustBox) {
    let x = (random::<usize>() % 10) + 1;
    rb.print(x, 3, rustbox::RB_BOLD, Color::White, Color::Black, "Press 'q' to quit.");
    rb.present()
}


fn main() {
    let options = [
        if stdio::stderr_raw().isatty() { Some(InitOption::BufferStderr) } else { None },
        ];
    let rbx = RustBox::init(&options).ok().unwrap();

    rbx.print(1, 1, rustbox::RB_BOLD, Color::White, Color::Black, "Hello, world!");
    rbx.present();
    let t = Duration::milliseconds(1);
    loop {
        match rbx.peek_event(t) {
            Ok(rustbox::Event::KeyEvent(_, _, ch)) => {
                match char::from_u32(ch) {
                    Some('q') => { break; },
                    _ => {}
                }
            },
            Err(e) => panic!("{}", e.description()),
            _ => { }
        }
        draw_quit_text(&rbx);
    }
}

